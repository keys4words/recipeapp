FROM python:3.7.3-slim

ENV PYTHONUNBUFFERED 1
COPY requirements.txt requirements.txt
RUN apt update && apt install libpq-dev python3-dev gcc -y
RUN python3 -m pip install --upgrade pip && python3 -m pip install --upgrade Pillow
RUN pip install -r requirements.txt

WORKDIR /app
COPY ./app .

RUN mkdir -p /vol/web/media
RUN mkdir -p /vol/web/static

RUN useradd keys4
RUN chown -R keys4:keys4 /vol/
RUN chmod -R 755 /vol/web

USER keys4